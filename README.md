# monorepo

[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-grey.svg)](https://conventionalcommits.org)
[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://lbesson.mit-license.org/)
[![Open Source? Yes!](https://badgen.net/badge/Open%20Source%20%3F/Yes%21/blue?icon=github)](https://github.com/Naereen/badges/)
[![pipeline status](https://gitlab.com/handmade-devops/monorepo/badges/master/pipeline.svg)](https://gitlab.com/handmade-devops/monorepo/commits/master)

A monorepo holding the complete architecture of [handmade-devops](https://gitlab.com/handmade-devops).

```sh
# Initialise and update the submodules
git submodule update --init --recursive

# run the local Deployment configuration
cd Deployment/Local
docker-compose up --build
```

## Register & start local GitLab runners

```sh
# Run docker-compose.ci.yml
docker volume create build-runner-config
docker-compose -f docker-compose.ci.yml up -d
# Register runner
docker run --rm -it -v build-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register

## Prune volumes from time to time
docker volume prune
```
